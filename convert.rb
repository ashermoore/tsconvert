
#convert files to mp4 using ffmpeg and move them to output
Dir.glob('stream/raw/*').each do |file|
  fileName = (0...8).map { (65 + rand(26)).chr }.join
  command = "ffmpeg -i #{file} -acodec copy -vcodec copy stream/output/#{fileName}.mp4"
  system command
  until File.exist?("stream/output/#{fileName}.mp4")
    sleep 0.5
  end
end
